/**
 * 
 */
package com.educlock.servicio;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.educlock.elemento.Jugador;

/**
 * @author luis
 *
 */
public class EduClockCRUD {
	
	//Agregar pool de conexiones, md5
	private String query = "";
	private Statement sentencia= null;
	private ResultSet rs = null;
	private int n = -1;
	
	//Procedimiento insertar jugador
	public void insertarJugador(Jugador jugador) throws SQLException {
		
		ServicioConexion servicioConexion =  new ServicioConexion();		
		sentencia = servicioConexion.conectar().createStatement();
			
		//sql para insertar en la tabla jugador de la base de datos
		query = "INSERT INTO `jugador`"
				+ "(`id_jugador`, `nombre_jugador`, `grado_escolar_id_grado_escolar`)" 
				+ "VALUES(``, `"+jugador.getNombreJugador()+"`, `"+jugador.getGradoJugador()+"`)";
		
		if (sentencia.executeUpdate(query) > 0){
			System.out.println("El registro se insertó exitosamente");
		}else{
			System.out.println("El registro se insertó exitosamente");
			System.out.println(query);
		}
		
		// el jugador incrementa su punteo en 10 puntos por indicar su nombre
		jugador.incrementarPunteoGlobal(10);		
	}
	
	/**
	 * Metodo for to save the score in the schema
	 * @throws SQLException 
	 */
	public void almacenarPunteo(Jugador jugador, int idActividad, int punteoAct, String fechaAct) throws SQLException{	
		
		//obtenemos el id del jugador
		n = consultarIdJugador(jugador); 

		ServicioConexion servicioConexion =  new ServicioConexion();		
		sentencia = servicioConexion.conectar().createStatement();
			
		//sql para insertar en tabla jugador_has_actividad	
		query = "INSERT INTO `db_edu_clock`.`jugador_has_actividad`"
				+ "(`jugador_id_jugador`, `actividad_id_actividad`, `punteo_actividad`, `fecha_actividad`)" 
				+ "VALUES(`"+ n +"`, `"+idActividad+"`, `"+punteoAct+"`, `"+fechaAct+"`)";
		
		if (sentencia.executeUpdate(query) > 0){
			System.out.println("El registro se insertó exitosamente");
		}else{
			System.out.println("El registro se insertó exitosamente");
			System.out.println(query);
		}
		
		
		

	}

	/**
	 * Metodo para consultar Id del Jugador
	 * @return 
	 * @throws SQLException 
	 */	
	private int consultarIdJugador(Jugador jugador) throws SQLException {
		// TODO Auto-generated method stub
		
		ServicioConexion servicioConexion =  new ServicioConexion();		
		sentencia = servicioConexion.conectar().createStatement();
		
		//sql para consultar usuario y contraseña en base de datos
		query = "SELECT `jugador`.`id_jugador`"
				+ "FROM `db_edu_clock`.`jugador`" 
				+ "WHERE `jugador`.`nombre_jugador` = `" + jugador.getNombreJugador() + "`;";
		
		try {
			rs = sentencia.executeQuery(query); 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(query);	
			e.printStackTrace();
		}

		if(rs != null){
			System.out.println("Identificador de jugador " + jugador.getNombreJugador() + " existe en base de datos");
			
			//obtenemos como String el valor de la columna contraseña de nuestra variable rs
			while(rs.next()){
			       n=rs.getInt("id_jugador");
			}
				return n;
			}else{
				System.out.println("Identificador de jugador " + jugador.getNombreJugador() + " no existe en base de datos");
				System.out.println(query);	
				return -1;
			}
		}
	
	/**
	 * Metodo for to validate credentials
	 * @return 
	 * @throws SQLException 
	 */	
	public boolean validarCredenciales(String usuarioAdmin, String contraAdmin) throws SQLException{
		
		ServicioConexion servicioConexion =  new ServicioConexion();		
		sentencia = servicioConexion.conectar().createStatement();
		
		//sql para consultar usuario y contraseña en base de datos
		query = "SELECT `usuario`.`contraseña_usuario`"
				+ "FROM `db_edu_clock`.`usuario`" 
				+ "WHERE `usuario`.`nombre_usuario` = `" + usuarioAdmin + "`;";
		
		ResultSet rs = null;
		String c = "";
		
		try {
			rs = sentencia.executeQuery(query);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(query);	
			e.printStackTrace();
		}

		if(rs != null){
			System.out.println("Usuario " + usuarioAdmin + " existe en base de datos");
			
			//obtenemos como String el valor de la columna contraseña de nuestra variable rs
			while(rs.next()){
			       c=rs.getString("contraseña_usuario");
			}
			
			if(c == contraAdmin){
				System.out.println("Contraseña para "+ usuarioAdmin + " correcta" );
				return true;
			}else{
				System.out.println("Contraseña para "+ usuarioAdmin + " incorrecta" );
				return false;
			}
		}else{
			System.out.println("Usuario " + usuarioAdmin + " no existe en base de datos");
			System.out.println(query);	
			return false;
		}	

	}
	
	public void consultarReporte(){
		//agregar sql para consultar datos de reporte inner join
		
	}
	
	/**
	 * Constructor de la clase
	 */
	public EduClockCRUD() {
		// TODO Auto-generated constructor stub
	}

}
