/**
 * 
 */
package com.educlock.servicio;
import java.sql.*;

/**
 * @author luis
 *
 */
public class ServicioConexion {
	
	/**
	 * @attributes of the class Reloj
	 */
	private static final String URL = "jdbc:mysql://localhost:3306";
	private static final String BD = "db_edu_clock";
	private static final String USUARIO_BD = "operador";
	private static final String CONTRASENA_BD = "$0p0rt cl16";
	public Connection conexion = null;

	/**
	 * Metodo conectar
	 */	
	@SuppressWarnings("finally")
	public Connection conectar() throws SQLException{
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conexion= DriverManager.getConnection(URL+BD, USUARIO_BD, CONTRASENA_BD);
			if (conexion != null){
				System.out.println("La conexión se ejecutó correctamente");
			}
		}catch(Exception e){
			// TODO: handle exception
			e.printStackTrace();
		}finally{
			return conexion;
		}
			
	};
	
	
}
