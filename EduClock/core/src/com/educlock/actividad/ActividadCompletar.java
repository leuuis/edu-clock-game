/**
 * 
 */
package com.educlock.actividad;

import java.util.ArrayList;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import com.educlock.elemento.Jugador;
import com.educlock.game.EduClockGame;
import com.educlock.servicio.EduClockCRUD;

/**
 * @author luis
 * 
 */
public class ActividadCompletar implements Actividad {

	static public ArrayList<String> conjuntoSolicitudesArmado = new ArrayList<String>();
	static public ArrayList<String> conjuntoRespuestasArmado = new ArrayList<String>();
	
	public int contadorActividad = -1;
	public boolean tmp = false;

	int punteoActividad = 0;
	
	// Getter punteo Actividad
	public int getPunteoActividad() {
		return punteoActividad;
	}
	
	private String valorPiezaSolicitada;
	
	public int seleccionAleatoria(ArrayList<String> arregloEntradaAleatoridad){
		
		int indiceAleatorio = -1;
		 
		Random seleccionador = new Random();
		
		/* size devuelve el número de elementos, restamos la unidad para obtener el ultimo indice, 
		 * recordar que se inicia en 0
		*/
		
		indiceAleatorio = seleccionador.nextInt(arregloEntradaAleatoridad.size());
		
		System.out.println("indice aleatorio: "+ indiceAleatorio + ", tamaño del arreglo: "+ arregloEntradaAleatoridad.size());
				
		return indiceAleatorio;

	}
	
	public String emitirSolicitudSiguiente(){		
		
			int indicePiezaSolicitada = -1;
		
			if (indicePiezaSolicitada == -1){
				
				// Obtenemos los valores a solicitar de forma aleatoria si aún no se ha hecho algúna solicitud
				indicePiezaSolicitada = seleccionAleatoria(EduClockGame.reloj.conjuntoPiezasReloj);			
				valorPiezaSolicitada = EduClockGame.reloj.conjuntoPiezasReloj.get(indicePiezaSolicitada);
				EduClockGame.reloj.conjuntoPiezasReloj.remove(indicePiezaSolicitada);
				
				
			} else if ( !(EduClockGame.reloj.conjuntoPiezasReloj.isEmpty()) ) {
				
				/* Si ya se realizó alguna solicitud obtenemos un indice aleatorio, y detenemos el proceso de obtener cuando su valor  
				 * no haya sido solicitado antes.
				 */
				while (conjuntoSolicitudesArmado.contains(valorPiezaSolicitada)) {
					
					indicePiezaSolicitada = seleccionAleatoria(EduClockGame.reloj.conjuntoPiezasReloj);
				}
				
				valorPiezaSolicitada = EduClockGame.reloj.conjuntoPiezasReloj.get(indicePiezaSolicitada);
			}
			
			conjuntoSolicitudesArmado.add(valorPiezaSolicitada);
			
			// Solicitamos la pieza
			System.out.println("Se obtuvo aleatoriamente la pieza "+ valorPiezaSolicitada + ".");
			
			return valorPiezaSolicitada;				
			
	};

	// Estos estan vacios ver si se pueden eliminar
	@Override
	public void verificarActividad(Jugador jugador, EduClockCRUD crud) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String obtenerFecha() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void responderItemActividad() {
				
		if (contadorActividad >=  3) {
			
			/* Si el contador de las actividades ha alcanzado 5 solicitudes es decir empezando de 0 a 4, se indica que ya se ha completado 
			 * el nivel del juego
			 */
			System.out.println("Ya has completado el nivel");
						
		}else if (contadorActividad < 3){
							
			// Incrementamos nuestro contador de actividad
			contadorActividad += 1;
			System.out.println("Pieza solicitada "+ conjuntoSolicitudesArmado.get(conjuntoSolicitudesArmado.size() - 1) + " pieza seleccionada "+ conjuntoRespuestasArmado.get(conjuntoRespuestasArmado.size() - 1));
			
			// Almacenamos en una variable booleana el resultado de comparar el string de la hora configurada con el string de la hora solicitada
			tmp = conjuntoRespuestasArmado.get((conjuntoRespuestasArmado.size() - 1)).equals(conjuntoSolicitudesArmado.get((conjuntoSolicitudesArmado.size() - 1)));
			System.out.println(tmp);
			
			// Si la comparación es verdadera se incrementa el contador de las respuestas correctas y se felicita al estudiante
			if (tmp){		

				EduClockGame.jugador.incrementarPunteoGlobal(5);
				System.out.println("¡Felicidades has seleccionado bien la pieza "+ conjuntoSolicitudesArmado.get(conjuntoSolicitudesArmado.size() - 1)  + "!"); 
				
				 // Inmediatamente después de alcanzar las 4 actividades se indica al estudiante que ha compleatdo el nivel
				if (contadorActividad >=  3) {
					
					System.out.println("Ya has completado el nivel");
				}
				
			//Si la comparación es falsa se indica al estudiante que ha configurado mal la hora
			}else{
						
				System.out.println("Lo siento, no has seleccionado bien la pieza "+ conjuntoSolicitudesArmado.get(conjuntoSolicitudesArmado.size() - 1));
				 
				// Inmediatamente después de alcanzar las 5 actividades se indica al estudiante que ha compleatdo el nivel
				if (contadorActividad >=  3) {
					
					/* Si el contador de las actividades ha alcanzado 4 solicitudes es decir empezando de 0 a 3, se indica que ya se ha completado 
					 * el nivel del juego
					 */
					System.out.println("Ya has completado el nivel");

				}
			}	
			
		}
	
		
	}
}
