/**
 * 
 */
package com.educlock.actividad;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Random;

import javax.swing.JOptionPane;

import com.educlock.elemento.Jugador;
import com.educlock.servicio.EduClockCRUD;

/**
 * @author luis
 *
 */

public class ActividadAnalogica implements Actividad {
	
	static protected ArrayList<String> arregloListaHoraPrimero = new ArrayList<String>();
	static protected ArrayList<String> arregloListaMinutoPrimero = new ArrayList<String>();
	public static ArrayList<String> arregloListaHorarioSolicitado = new ArrayList<String>();
	public static ArrayList<String> arregloListaHorarioConfigurado = new ArrayList<String>();

	Integer numeroRutaHorera= 11;
	Integer numeroRutaMinutera=0;
	public int contadorActividad = 0;
	private int contadorCorrectas = 0;
	public Integer punteoActividad = 0;
	private String valorHoraSolicitada = "";
	private String valorMinutoSolicitado = "";
	@SuppressWarnings("unused")
	private String fechaAct = "";
	
	// variables para conversion de int a String
	private boolean tmp;
	
	//Método constructor
	public ActividadAnalogica() {
		
	}
	
	//Métodos y operaciones
	
	public void inicializarActividadAnalogica(){
		
		//agregamos los objetos String que forman el horario para primero
		arregloListaHoraPrimero.add("01");
		arregloListaHoraPrimero.add("02");
		arregloListaHoraPrimero.add("03");
		arregloListaHoraPrimero.add("04");
		arregloListaHoraPrimero.add("05");
		arregloListaHoraPrimero.add("06");
		arregloListaHoraPrimero.add("07");
		arregloListaHoraPrimero.add("08");
		arregloListaHoraPrimero.add("09");
		arregloListaHoraPrimero.add("10");
		arregloListaHoraPrimero.add("11");
		arregloListaHoraPrimero.add("12");
		
		System.out.println("Horas Primero");
		for(int i=0; i<12; i++){					
			System.out.println(arregloListaHoraPrimero.get(i));
       }
			
		arregloListaMinutoPrimero.add("00");
		arregloListaMinutoPrimero.add("30");	
				
		System.out.println("Minutos Primero");
		for(int i=0; i<2; i++){					
			System.out.println(arregloListaMinutoPrimero.get(i));
       }
		
	};

	public Integer aumentarHorera(){
		
		if (numeroRutaHorera < 11){
			numeroRutaHorera += 1; 	
		}else{
			numeroRutaHorera = 0;
		}
	
		return numeroRutaHorera;

	}
	
	public Integer disminuirHorera(){
		
		if (numeroRutaHorera > 0){
			numeroRutaHorera -= 1; 	
		}else{
			numeroRutaHorera = 11;
		}
		
		return numeroRutaHorera;
		
	}
	
	public Integer aumentarMinutera(){
		
		if (numeroRutaMinutera == 0){
			numeroRutaMinutera += 1;  	
		}else{
			numeroRutaMinutera = 0;
		}
		
		return numeroRutaMinutera;
		
	}
	
	public Integer disminuirMinutera(){

		if (numeroRutaMinutera == 1){
			numeroRutaMinutera -= 1;  	
		}else{
			numeroRutaMinutera = 1;
		}	
	
		return numeroRutaMinutera;
	
	}

	public int seleccionAleatoria(ArrayList<String> arregloEntradaAleatoridad){
		
		int indiceAleatorio = -1;
		 
		Random seleccionador = new Random();
		
		/* size devuelve el número de elementos, restamos la unidad para obtener el ultimo indice, 
		 * recordar que se inicia en 0
		*/
		
		indiceAleatorio = seleccionador.nextInt(arregloEntradaAleatoridad.size());
		
		System.out.println("indice aleatorio: "+ indiceAleatorio + ", tamaño del arreglo: "+ arregloEntradaAleatoridad.size());
				
		return indiceAleatorio;

	}

	public String emitirSolicitudSiguiente(){

		int indexHoraPrimero = -1;
		int indexMinutoPrimero = -1;

		if (contadorActividad <  5) {
			
		//obtenemos los indices de forma aleatoria
		indexHoraPrimero = seleccionAleatoria(arregloListaHoraPrimero);
		indexMinutoPrimero = seleccionAleatoria(arregloListaMinutoPrimero);
		
		//obtenemos los valores de los indices aleatorios
		valorHoraSolicitada = arregloListaHoraPrimero.get(indexHoraPrimero);
		valorMinutoSolicitado = arregloListaMinutoPrimero.get(indexMinutoPrimero);
		
		//llenamos el arreglo del horario solicitado
		arregloListaHorarioSolicitado.add(valorHoraSolicitada + ":"+ valorMinutoSolicitado);
		
		//solicitamos el horario
		System.out.println("Por favor configure la hora " + arregloListaHorarioSolicitado.get(arregloListaHorarioSolicitado.size() - 1) 
							+" en el reloj analógico aumentando o disminuyendo la aguja horera y la aguja minutera");

		}
		return arregloListaHorarioSolicitado.get(arregloListaHorarioSolicitado.size() - 1);
		
	};
	
	@Override
	public void responderItemActividad() {
		
		/* Variables de tipo string que permitiran almacenar los últimos valores del arreglo que almacena las horas configuradas y del arreglo que
		 * almacena las horas solicitadas
		 */
		String valorHoraConfigurada;
		String valorMinutoConfigurado;
		
		if (contadorActividad >=  5) {
			
			/* Si el contador de las actividades ha alcanzado 5 solicitudes es decir empezando de 0 a 4, se indica que ya se ha completado 
			 * el nivel del juego
			 */
			System.out.println("Ya haz completado el nivel");
			JOptionPane.showMessageDialog(null, "Ya haz completado el nivel");
						
		}else if (contadorActividad <  5){
						
			// Obtenemos los valores que corresponden a las rutas (indice)
			valorHoraConfigurada = arregloListaHoraPrimero.get(numeroRutaHorera);
			valorMinutoConfigurado = arregloListaMinutoPrimero.get(numeroRutaMinutera);
			
			// Llenamos el arreglo del horario configurado
			arregloListaHorarioConfigurado.add(valorHoraConfigurada + ":"+ valorMinutoConfigurado);
			
			// Incrementamos nuestro contador de actividad
			contadorActividad += 1;
			System.out.println("Hora solicitada "+ arregloListaHorarioSolicitado.get(arregloListaHorarioSolicitado.size() - 1) + " hora configurada "+ arregloListaHorarioConfigurado.get(arregloListaHorarioConfigurado.size() - 1));
			
			// Almacenamos en una variable booleana el resultado de comparar el string de la hora configurada con el string de la hora solicitada
			tmp = arregloListaHorarioConfigurado.get((arregloListaHorarioConfigurado.size() - 1)).equals(arregloListaHorarioSolicitado.get((arregloListaHorarioSolicitado.size() - 1)));
			System.out.println(tmp);
			
			// Si la comparación es verdadera se incrementa el contador de las respuestas correctas y se felicita al estudiante
			if (tmp){		
				
				contadorCorrectas += 1;
				punteoActividad = (contadorCorrectas * 6);
				System.out.println("¡Felicidades has escrito bien la hora "+ arregloListaHorarioSolicitado.get(arregloListaHorarioSolicitado.size() - 1));
				JOptionPane.showMessageDialog(null, "¡Felicidades has escrito bien la hora "+ arregloListaHorarioSolicitado.get(arregloListaHorarioSolicitado.size() - 1) +"!");
				
				// Inmediatamente después de alcanzar las 5 actividades se indica al estudiante que ha compleatdo el nivel
				if (contadorActividad >=  5) {
					
					/* Si el contador de las actividades ha alcanzado 5 solicitudes es decir empezando de 0 a 4, se indica que ya se ha completado 
					 * el nivel del juego
					 */
					System.out.println("Ya haz completado el nivel");
					JOptionPane.showMessageDialog(null, "Ya haz completado el nivel");
				}
				
			//Si la comparación es falsa se indica al estudiante que ha configurado mal la hora
			}else{
				
				System.out.println("Lo siento, no has colocado bien la hora "+ arregloListaHorarioSolicitado.get(arregloListaHorarioSolicitado.size() - 1));
				JOptionPane.showMessageDialog(null, "Lo siento, no has escrito bien la hora "+ arregloListaHorarioSolicitado.get(arregloListaHorarioSolicitado.size() - 1) );
				
				// Inmediatamente después de alcanzar las 5 actividades se indica al estudiante que ha compleatdo el nivel
				if (contadorActividad >=  5) {
					
					/* Si el contador de las actividades ha alcanzado 5 solicitudes es decir empezando de 0 a 4, se indica que ya se ha completado 
					 * el nivel del juego
					 */
					System.out.println("Ya haz completado el nivel");
					JOptionPane.showMessageDialog(null, "Ya haz completado el nivel");
				}
			}	
			
		}
		
	}
	
	public void verificarActividad(){
		
		/** comparar los dos arreglos configurado y solicitado, 
		 *  aumentar un contador si los valores son iguales en cada indice
		 **/
		/* jugador.incrementarPunteoGlobal((contadorActividad * 6));
		System.out.println(jugador.getPunteoGlobal());
				
		fechaAct = obtenerFecha();
		
		/*
		try {
			crud.almacenarPunteo(jugador, 2, (contadorActividad * 6), fechaAct);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
	}

	@Override
	public String obtenerFecha() {
		// TODO Auto-generated method stub
		String fechaSistema;
		int anyo;
		int mes;
		int dia;		

		GregorianCalendar calendario = new GregorianCalendar();
		anyo = calendario.get(GregorianCalendar.YEAR);
		mes = calendario.get(GregorianCalendar.MONTH);
		dia = calendario.get(GregorianCalendar.DATE);

		fechaSistema = anyo +"-"+ mes +"-"+ dia; 
		System.out.println (fechaSistema);	
		
		return fechaSistema;
	}

	@Override
	public void verificarActividad(Jugador jugador, EduClockCRUD crud) {
		// TODO Auto-generated method stub
		
	}

}