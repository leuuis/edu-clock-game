/**
 * 
 */
package com.educlock.actividad;

import java.util.ArrayList;

import com.educlock.elemento.Jugador;
import com.educlock.servicio.EduClockCRUD;

/**
 * @author luis
 *
 */
interface Actividad {
	
	public int seleccionAleatoria(ArrayList<String> arregloEntradaAleatoridad);
	public String emitirSolicitudSiguiente();
	public void verificarActividad(Jugador jugador, EduClockCRUD crud);
	public String obtenerFecha();
	public void responderItemActividad();
	
}
