/**
 * 
 */
package com.educlock.elemento;

import java.util.ArrayList;

/**
 * @author luis
 *
 */
public class Reloj {
	
	public ArrayList<String> conjuntoPiezasReloj =  new ArrayList<String>();
	
	/**
	 * Constructor of the class Reloj
	 */	
	public Reloj() {

	}
	
	//inicialización del array string
	public void inicializarReloj(){
		//agregamos los objetos string que forman parte del reloj
		conjuntoPiezasReloj.add("Fondo");	
		conjuntoPiezasReloj.add("Numerales");	
		conjuntoPiezasReloj.add("Horera");	
		conjuntoPiezasReloj.add("Minutera");	
		
		System.out.println("Piezas Reloj");
		for(int i=0; i<2; i++){					
			System.out.println(conjuntoPiezasReloj.get(i));
       }
		
	}

}
