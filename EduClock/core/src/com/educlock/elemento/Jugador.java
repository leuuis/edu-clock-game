/**
 * 
 */
package com.educlock.elemento;

/**
 * @author luis
 *
 */
public class Jugador {
	/**
	 * @attributes of the class Jugador
	 */
	private int gradoJugador = 0;
	private String nombreJugador = "";
	private int punteoGlobal = 0;
	
	public Jugador() {
		// TODO Auto-generated constructor stub
	}

	/** 
	 * Getters an Setters 
	 */
	/**
	 * @return the gradoJugador
	 */
	public int getGradoJugador() {
		return gradoJugador;
	}

	/**
	 * @param gradoJugador the gradoJugador to set
	 */
	public void setGradoJugador(int gradJugador) {
		this.gradoJugador = gradJugador;
	}

	/**
	 * @return the nombreJugador
	 */
	public String getNombreJugador() {
		return nombreJugador;
	}

	/**
	 * @param nombreJugador the nombreJugador to set
	 */
	public void setNombreJugador(String nomJugador) {
		this.nombreJugador = nomJugador;
	}

	/**
	 * @return the punteoGlobal
	 */
	public int getPunteoGlobal() {
		return punteoGlobal;
	}

	/**
	 * @param punteoGlobal the punteoGlobal to set
	 */
	public void incrementarPunteoGlobal(int punteoGlbl) {
		this.punteoGlobal += punteoGlbl;
	}
}
