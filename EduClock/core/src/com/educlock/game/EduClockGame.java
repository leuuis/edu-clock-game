package com.educlock.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.educlock.elemento.Jugador;
import com.educlock.elemento.Reloj;
import com.educlock.servicio.ServicioConexion;


public class EduClockGame extends Game {
	
	// Variables para las posiciones x e y recalculadas para centrar
 	public static int width = 1024;
 	public static int height = 768;
		
	// usaremos el mismo sprite batch para todas las clases graficas.
	public SpriteBatch batch;
	
	protected Pantalla scrActividadAnalogicaCamara;
	protected Pantalla scrDatosJugadorCamara;
	protected Pantalla scrMenuCamara;
	protected Pantalla scrDatosActividadArmadoCamara;

	private static Texture txrFondoReloj;
	private static Texture txrNumeroReloj;
	private static Texture txrHorera01;
	private static Texture txrHorera02;
	private static Texture txrHorera03;
	private static Texture txrHorera04;
	private static Texture txrHorera05;
	private static Texture txrHorera06;
	private static Texture txrHorera07;
	private static Texture txrHorera08;
	private static Texture txrHorera09;
	private static Texture txrHorera10;
	private static Texture txrHorera11;
	private static Texture txrHorera12;

	private static Texture txrMinutera00;
	private static Texture txrMinutera30;
	
	static Sprite sptFondoReloj;
	static Sprite sptMinutera;
	static Sprite sptHorera;
	static Sprite sptNumeroReloj;
		
	public static Reloj reloj = new Reloj();
	public static Jugador jugador = new Jugador();
	static ServicioConexion conexion = new ServicioConexion();
	
	static Skin skin;
		
	@Override
	public void create () {  
		
		batch = new SpriteBatch();

/*		
 * scrMenuCamara = new PantallaEscenaMenuCamara(this);	
 * scrDatosJugadorCamara = new PantallaEscenaDatosJugadorCamara(this);
 * scrDatosActividadArmadoCamara = new PantallaEscenaActividadArmadoCamara(this);
 * scrActividadAnalogicaCamara = new PantallaEscenaActividadAnalogicaCamara(this);
 */ 
		scrDatosActividadArmadoCamara = new PantallaEscenaActividadArmadoCamara(this);	
		setScreen(scrDatosActividadArmadoCamara);
				
 		// Configuramos una piel o una apariencia que abarca formato de botones, fondo, tamaño, tipografía, etc.
	 	skin = new Skin(Gdx.files.internal("uiskin.json"));
		
		txrFondoReloj = new Texture ("fondoReloj/750x563/fondoReloj.png");
 		txrNumeroReloj = new Texture ("numeroReloj/750x563/numeroReloj.png");
 		
 		txrMinutera00 = new Texture ("minutera/750x563/00.png");
 		txrMinutera30 = new Texture ("minutera/750x563/30.png");
 		
 		txrHorera01 = new Texture ("horera/750x563/01.png");
 		txrHorera02 = new Texture ("horera/750x563/02.png");
 		txrHorera03 = new Texture ("horera/750x563/03.png");
 		txrHorera04 = new Texture ("horera/750x563/04.png");
 		txrHorera05 = new Texture ("horera/750x563/05.png");
 		txrHorera06 = new Texture ("horera/750x563/06.png");
 		txrHorera07 = new Texture ("horera/750x563/07.png");
 		txrHorera08 = new Texture ("horera/750x563/08.png");
 		txrHorera09 = new Texture ("horera/750x563/09.png");
 		txrHorera10 = new Texture ("horera/750x563/10.png");
 		txrHorera11 = new Texture ("horera/750x563/11.png");
 		txrHorera12 = new Texture ("horera/750x563/12.png");
 		 		
 		sptFondoReloj = new Sprite (txrFondoReloj);
 		sptNumeroReloj = new Sprite (txrNumeroReloj);
 		sptMinutera = new Sprite (txrMinutera00);
 		sptHorera = new Sprite (txrHorera12);
 		
	}

	public void dispose(){
		
		skin.dispose();
		
		txrFondoReloj.dispose();
 		txrNumeroReloj.dispose();
 		
 		txrMinutera00.dispose();
 		txrMinutera30.dispose();
 		
 		txrHorera01.dispose();
 		txrHorera02.dispose();
 		txrHorera03.dispose();
 		txrHorera04.dispose();
 		txrHorera05.dispose();
 		txrHorera06.dispose();
 		txrHorera07.dispose();
 		txrHorera08.dispose();
 		txrHorera09.dispose();
 		txrHorera10.dispose();
 		txrHorera11.dispose();
 		txrHorera12.dispose();
 		
 		scrDatosActividadArmadoCamara.dispose();
	}
}
