/**
 * 
 */
package com.educlock.game;

import com.badlogic.gdx.Screen;

/**
 * @author luis
 *
 */
public abstract class Pantalla implements Screen {

	protected EduClockGame game;
	// Pasamos EduClockGame a pantalla para amarrar las diferentes pantallas.
	
	public Pantalla(EduClockGame game) {
		// TODO Auto-generated constructor stub
		this.game = game;	
		
	}

	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		
	}	

	@Override
	public void resize(int width, int height) {
		System.out.println("Ancho: "+ width + " Alto: " + height);
		
	}

	
	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
