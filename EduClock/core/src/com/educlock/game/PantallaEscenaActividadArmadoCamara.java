package com.educlock.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.educlock.actividad.ActividadCompletar;

public class PantallaEscenaActividadArmadoCamara extends Pantalla {
	
	/* Declaramos una camara ortográfica que nos permitira ver los objetos del 
	 * mismo tamaño y así mejorar la resolución de las imagenes al redimensionar
	 * la pantalla del juego
	*/
	private OrthographicCamera camArmado;

	private Stage escenarioArmado;
 	
	// Tabla para contener los botones de control
	private Table tblMenuArmado = new Table();
		
 	// Variables booleanas para el estado de las piezas del reloj
 	private boolean bolFondoReloj = false;
 	private boolean bolNumeroReloj = false;
 	private boolean bolMinuteraReloj = false;
 	private boolean bolHoreraReloj = false;
 	
 	// Inicializamos y asignamos textura a los botones
	private Texture txrUpBtnReloj   = new Texture(Gdx.files.internal("boton/imgBtnReloj/image_up.png"));
	private Texture txrDownBtnReloj = new Texture(Gdx.files.internal("boton/imgBtnReloj/image_down.png"));
	private Texture txrBckBtnReloj  = new Texture(Gdx.files.internal("boton/imgBtnReloj/background.png"));
	private MiBoton mbnFondoReloj   = new MiBoton (txrUpBtnReloj, txrDownBtnReloj, txrBckBtnReloj);
	
	private Texture txrUpBtnNumeros   = new Texture(Gdx.files.internal("boton/imgBtnNumeros/image_up.png"));
	private Texture txrDownBtnNumeros = new Texture(Gdx.files.internal("boton/imgBtnNumeros/image_down.png"));
	private Texture txrBckBtnNumeros  = new Texture(Gdx.files.internal("boton/imgBtnNumeros/background.png"));
	private MiBoton mbnNumeroReloj   = new MiBoton (txrUpBtnNumeros, txrDownBtnNumeros, txrBckBtnNumeros);

	private Texture txrUpBtnHorera   = new Texture(Gdx.files.internal("boton/imgBtnHorera/image_up.png"));
	private Texture txrDownBtnHorera = new Texture(Gdx.files.internal("boton/imgBtnHorera/image_down.png"));
	private Texture txrBckBtnHorera  = new Texture(Gdx.files.internal("boton/imgBtnHorera/background.png"));
	private MiBoton mbnHoreraReloj   = new MiBoton (txrUpBtnHorera, txrDownBtnHorera, txrBckBtnHorera);
	
	private Texture txrUpBtnMinutera   = new Texture(Gdx.files.internal("boton/imgBtnMinutera/image_up.png"));
	private Texture txrDownBtnMinutera = new Texture(Gdx.files.internal("boton/imgBtnMinutera/image_down.png"));
	private Texture txrBckBtnMinutera  = new Texture(Gdx.files.internal("boton/imgBtnMinutera/background.png"));
	
	private MiBoton mbnMinuteraReloj   = new MiBoton (txrUpBtnMinutera, txrDownBtnMinutera, txrBckBtnMinutera);	
	
 	// Instancia de clase para tratamiento de datos de la ActividadCompletar.
	private ActividadCompletar actividadCompletar = new ActividadCompletar();
	
	private String strPunteoGlobal = Integer.toString(EduClockGame.jugador.getPunteoGlobal());
	 
	private Skin skin;
	private Label lblPunteo;
	
	
	private static Texture txrFeliz;
	private static Texture txrSonriente;
	private static Texture txrTriste;
	
	static Image imgFeliz;
	static Image imgTriste;
	static Image imgSonriente;
		
	private static Dialog dlgFeliz;
	private static Dialog dlgTriste;
	private static Dialog dlgPantallaSig;
	
	private static Label lblFeliz;
	private static Label lblTriste;
	private static Label lblPantallaSig;
	
	// Sonidos para los avisos
	private static Sound sndFeliz = Gdx.audio.newSound(Gdx.files.internal("audio/good.mp3"));
	private static Sound sndTriste = Gdx.audio.newSound(Gdx.files.internal("audio/fail.mp3"));
	private static Sound sndPantallaSig = Gdx.audio.newSound(Gdx.files.internal("audio/success.mp3"));

 	public PantallaEscenaActividadArmadoCamara(final EduClockGame game) {
 		super(game);
 			 		 		
 		// Inicializamos el escenarioArmado 
 		escenarioArmado = new Stage(new ExtendViewport (1024, 768));
 		
		// Configuramos una piel o una apariencia que abarca formato de botones, fondo, tamaño, tipografía, etc.
		skin = new Skin(Gdx.files.internal("uiskin.json"));
		
		// Etiqueta para solicitar la pieza
		final Label lblSolicitudPieza = new Label("", skin);
		 lblPunteo = new Label("", skin);
		
		// Texturas para los avisos
	 	txrFeliz = new Texture ("carita/feliz.png");
 		txrTriste = new Texture ("carita/triste.png");
 		txrSonriente = new Texture ("carita/sonriente.png");
 		
 		// Imagenes que contienen las texturas para los avisos
 		imgFeliz = new Image(txrFeliz);
 		imgTriste = new Image(txrTriste);
 		imgSonriente = new Image(txrSonriente);
	 		
		// Inicializamos el objeto reloj
		 EduClockGame.reloj.inicializarReloj();

 		// Inicializamos los sonidos para los avisos
		sndFeliz = Gdx.audio.newSound(Gdx.files.internal("audio/good.mp3"));
		sndTriste = Gdx.audio.newSound(Gdx.files.internal("audio/fail.mp3"));
		sndPantallaSig = Gdx.audio.newSound(Gdx.files.internal("audio/success.mp3"));
		
		// Botones para cerrar los avisos
		TextButton txbAceptardlgFeliz = new TextButton("Aceptar ", skin);
		TextButton txbAceptardlgTriste = new TextButton("Aceptar ", skin);
		TextButton txbAceptardlgPantallaSig = new TextButton("Aceptar ", skin);
		
		// Mensajes de los avisos
		lblFeliz = new Label("Tu respuesta es correcta.", skin);
		lblTriste = new Label("Tu respuesta es incorrecta.", skin);
		lblPantallaSig = new Label("Pasemos a otra pantalla.", skin);
		
		// Dialogo de felicitación
 		dlgFeliz = new Dialog ("FELICIDADES", skin);
 		dlgFeliz.add(imgFeliz);
 		dlgFeliz.add(lblFeliz);
 		dlgFeliz.row().colspan(3);
 		dlgFeliz.add(txbAceptardlgFeliz);
 		dlgFeliz.debug();	
		
 		// Escuchador del botón para cerrar dialogo de felicitación
 		txbAceptardlgFeliz.addListener(new ChangeListener(){

			@Override
			public void changed(ChangeEvent event, Actor actor) {
					sndFeliz.stop();
	 				escenarioArmado.getRoot().removeActor(dlgFeliz);
	 				
	 				 // Inmediatamente después de alcanzar las 4 actividades se indica al estudiante que ha compleatdo el nivel
	 				if (actividadCompletar.contadorActividad >=  3) {

	 					dlgPantallaSig.show(escenarioArmado);
	 					sndPantallaSig.play();
	 					
	 				}
			}
		
 		});
	 	
 		// Dialogo de equivocación
 		dlgTriste= new Dialog ("LO SIENTO", skin);
 		dlgTriste.add(imgTriste);
 		dlgTriste.add(lblTriste);
 		dlgTriste.row().colspan(3);
 		dlgTriste.add(txbAceptardlgTriste);
 		dlgTriste.debug();
	 		
 		// Escuchador del botón para cerrar dialogo de equivocación
 		txbAceptardlgTriste.addListener(new ChangeListener(){

			@Override
			public void changed(ChangeEvent event, Actor actor) {
					sndTriste.stop();
	 				escenarioArmado.getRoot().removeActor(dlgTriste);
	 				
	 				 // Inmediatamente después de alcanzar las 4 actividades se indica al estudiante que ha compleatdo el nivel
	 				if (actividadCompletar.contadorActividad >=  3) {

	 					dlgPantallaSig.show(escenarioArmado);
	 					sndPantallaSig.play();
	 				}
			}
 			
 		});

 		// Dialogo de pantalla siguiente
 		dlgPantallaSig= new Dialog ("BIEN HECHO", skin);
 		dlgPantallaSig.add(imgSonriente);
 		dlgPantallaSig.add(lblPantallaSig);
 		dlgPantallaSig.row().colspan(3);
 		dlgPantallaSig.add(txbAceptardlgPantallaSig);
 		dlgPantallaSig.debug();
 		
 		// Escuchador del botón para cerrar dialogo de pantalla siguiente
 		txbAceptardlgPantallaSig.addListener(new ChangeListener(){

			@Override
			public void changed(ChangeEvent event, Actor actor) {
					sndPantallaSig.stop();
	 				escenarioArmado.getRoot().removeActor(dlgPantallaSig);
	 				
	 				// Cambiar a pantalla siguiente
 					disposse();
			}
 			
 		});
		
 		// Agregamos la solicitud inicial
 		lblSolicitudPieza.setText(actividadCompletar.emitirSolicitudSiguiente());
 		System.out.println(EduClockGame.reloj.conjuntoPiezasReloj.get(0));
 		
 		// Etablecemos el texto de la etiqueta punteo
 		lblPunteo.setText(strPunteoGlobal);
 		
 		// Configuramos el color y posición de la etiqueta que mostrará la solicitud y punteo.
 		lblSolicitudPieza.setPosition(100, 100);
 		lblSolicitudPieza.setColor(0, 0, 0, 1);
 		lblPunteo.setPosition(500, 500);
 		lblPunteo.setColor(0, 0, 0, 1);
		
 		//	Agregamos los botones 		
 		tblMenuArmado.add(mbnFondoReloj).padLeft(5).padTop(5).padBottom(5);
 		tblMenuArmado.add(mbnNumeroReloj).padLeft(5).padTop(5).padBottom(5);
 		tblMenuArmado.add(mbnHoreraReloj).padLeft(5).padTop(5).padBottom(5);
 		tblMenuArmado.add(mbnMinuteraReloj).padLeft(5).padRight(5).padTop(5).padBottom(5);
 		
 		tblMenuArmado.debug();
 		
 		// Agregamos elementos al escenario de la actividad Armado u Completar.
 		escenarioArmado.addActor(tblMenuArmado);
 		escenarioArmado.addActor(lblSolicitudPieza);
 		escenarioArmado.addActor(lblPunteo);
 		
 		Gdx.input.setInputProcessor(escenarioArmado);
 		
 		mbnFondoReloj.addListener(new InputListener(){
 			//si hacemos clic en el actor botón, se colorea de cyan
 			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button){
 				
 				if (actividadCompletar.contadorActividad < 3){
 					
	 				System.out.println("touchDown botón fondo");
	 				
	 				EduClockGame.sptFondoReloj.setPosition(220, 60);
	 				ActividadCompletar.conjuntoRespuestasArmado.add("Fondo");
	 				System.out.println("Se agregó la respuesta Fondo");	 				 				
	 				actividadCompletar.responderItemActividad();
	 				
	 				// Si la respuesta es correcta mostraremos el fondo
	 				if (actividadCompletar.tmp){
	 					bolFondoReloj = true;	
	 				}
	 				
	 				seleccionarAviso();
	 				strPunteoGlobal = Integer.toString(EduClockGame.jugador.getPunteoGlobal());
	 				lblPunteo.setText(strPunteoGlobal);
 				}
 				
 				return true;
 			}
 			
 			public void touchUp (InputEvent event, float x, float y, int pointer, int button){
 				
 				System.out.println("touchUp botón fondo");

 				if ( !(EduClockGame.reloj.conjuntoPiezasReloj.isEmpty()) ){
 					lblSolicitudPieza.setText(actividadCompletar.emitirSolicitudSiguiente());	
 				}else {
 					lblSolicitudPieza.setText("--");
 				} 
 			}
 		});
 		
 		mbnNumeroReloj.addListener(new InputListener(){
 			//si hacemos clic en el actor botón, se colorea de cyan
 			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button){
 				
 				if (actividadCompletar.contadorActividad < 3){
 					
 				System.out.println("touchDown botón números");
 				
	 				EduClockGame.sptNumeroReloj.setPosition(220, 60);
	 				ActividadCompletar.conjuntoRespuestasArmado.add("Numerales");
	 				System.out.println("Se agregó la respuesta Números");
	 				actividadCompletar.responderItemActividad();
	 				
	 				// Si la respuesta es correcta mostraremos el fondo
	 				if (actividadCompletar.tmp){
		 				bolNumeroReloj = true;
	 				}
	 				
	 				seleccionarAviso();
	 				strPunteoGlobal = Integer.toString(EduClockGame.jugador.getPunteoGlobal());
	 				lblPunteo.setText(strPunteoGlobal);

 				}
 				return true;
 			}
 			
 			public void touchUp (InputEvent event, float x, float y, int pointer, int button){
 				System.out.println("touchUp botón números");
 				
 				if ( !(EduClockGame.reloj.conjuntoPiezasReloj.isEmpty()) ){
 					lblSolicitudPieza.setText(actividadCompletar.emitirSolicitudSiguiente());	
 				}else {
 					lblSolicitudPieza.setText("--");
 				} 				
 			}
 		});
 		
 		mbnMinuteraReloj.addListener(new InputListener(){
 			//si hacemos clic en el actor botón, se colorea de cyan
 			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button){
 				
 				if (actividadCompletar.contadorActividad < 3){
 					
	 				System.out.println("touchDown botón minutera");
	 				
	 				EduClockGame.sptMinutera.setPosition(220, 60);
	 				ActividadCompletar.conjuntoRespuestasArmado.add("Minutera");
	 				System.out.println("Se agregó la respuesta Minutera");
	 				actividadCompletar.responderItemActividad();
	 				
	 				// Si la respuesta es correcta mostraremos el fondo
	 				if (actividadCompletar.tmp){
		 				bolMinuteraReloj = true;
	 				}

	 				seleccionarAviso();
	 				strPunteoGlobal = Integer.toString(EduClockGame.jugador.getPunteoGlobal());
	 				lblPunteo.setText(strPunteoGlobal);

 				}
 				return true;
 			}
 			
 			public void touchUp (InputEvent event, float x, float y, int pointer, int button){
 				
 				System.out.println("touchUp botón minutera");
 				
 				if ( !(EduClockGame.reloj.conjuntoPiezasReloj.isEmpty()) ){
 					lblSolicitudPieza.setText(actividadCompletar.emitirSolicitudSiguiente());	
 				}else {
 					lblSolicitudPieza.setText("--");
 				} 
 			}
 		});
 		
 		mbnHoreraReloj.addListener(new InputListener(){
 			//si hacemos clic en el actor botón, se colorea de cyan
 			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button){
 				
 				if (actividadCompletar.contadorActividad < 3){
 					
 	 				System.out.println("touchDown botón horera");
 	 				
 	 				EduClockGame.sptHorera.setPosition(220, 60);
 	 				ActividadCompletar.conjuntoRespuestasArmado.add("Horera");
 	 				System.out.println("Se agregó la respuesta Horera");
 	 				
 	 				actividadCompletar.responderItemActividad();
	 				
	 				// Si la respuesta es correcta mostraremos el fondo
	 				if (actividadCompletar.tmp){
		 				bolHoreraReloj = true;
	 				}

	 				seleccionarAviso();
	 				strPunteoGlobal = Integer.toString(EduClockGame.jugador.getPunteoGlobal());
	 				lblPunteo.setText(strPunteoGlobal);

 				}

 				return true;
 			}
 			
 			public void touchUp (InputEvent event, float x, float y, int pointer, int button){
 				System.out.println("touchUp botón horera");
 				
 				if ( !(EduClockGame.reloj.conjuntoPiezasReloj.isEmpty()) ){
 					lblSolicitudPieza.setText(actividadCompletar.emitirSolicitudSiguiente());	
 				}else {
 					lblSolicitudPieza.setText("--");
 				} 
 			}
 		});
 			
 	}
 
 	public void seleccionarAviso (){
 		
 		if (actividadCompletar.tmp){	

			dlgFeliz.show(escenarioArmado);
			sndFeliz.play();
			
		//Si la comparación es falsa se indica al estudiante que ha configurado mal la hora
		}else{
					
			dlgTriste.show(escenarioArmado);
			sndTriste.play();			
		}	
 	}
 	
 	@Override
 	public void show () {  
 		
 		/* A la camara ortográfica le pasaremos el alto y el ancho de la
 		 *  pantalla
 		 */
 			 		
 		camArmado= new OrthographicCamera(EduClockGame.width, EduClockGame.height);
 	}
 
 	@Override
 	public void render(float delta) {
 		renderizarPantallaActividadArmado(); 		
 		escenarioArmado.draw();
 		escenarioArmado.act();	 	
 	}
 	
 	public void renderizarPantallaActividadArmado(){
 		
 		// borra la pantalla del color indicado en rgb entre 0-1 flotantes
 		Gdx.gl.glClearColor(1, 0.8f, 0.2f, 1); 
 		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
 		
 		//Le pasamos la matriz de la camara al sprite batch
 		game.batch.setProjectionMatrix(camArmado.combined);
 		
 		game.batch.begin();		// usamos el mismo sprite batch de EduClockGame	
 		strPunteoGlobal = Integer.toString(EduClockGame.jugador.getPunteoGlobal());
 		lblPunteo.setText(strPunteoGlobal);
 		
 		if (bolFondoReloj){
 			EduClockGame.sptFondoReloj.draw(game.batch);
 		}

 		if (bolNumeroReloj){
 			EduClockGame.sptNumeroReloj.draw(game.batch);
 		}
 		
 		if (bolMinuteraReloj){
 			EduClockGame.sptMinutera.draw(game.batch);
 		}
 		
 		if (bolHoreraReloj){
 			EduClockGame.sptHorera.draw(game.batch);
 		}	
 		
 		game.batch.end();	
 	}
 	
 	/* re dimensionar el tamaño de la pantalla, para que automaticamente la
 	 * camara también cambien su tamaño. 
 	 */
 	@Override
 	public void resize(int width, int height) {
 		 		
 		EduClockGame.width = width;
 		EduClockGame.height = height;
 		
 		// Para que la tabla tenga una posición proporcional al tamño de la ventana.
 		tblMenuArmado.setPosition( ((int)(EduClockGame.width * 0.78125)),((int) (EduClockGame.height * 0.09765625)));
 		
 		/* setToOrtho sirve para cambiar el tamaño de la cámara
 		 * si el parámetro yDown es falso los ejes de cordenadas se
 		 * inicia abajo a la izquierda de la pantalla
 		 */
 		camArmado.setToOrtho(false, EduClockGame.width, EduClockGame.height);
 		
 		escenarioArmado.getViewport().update(width, height);
 		
 		System.out.println(((int)(EduClockGame.width * 0.78125)));
 		System.out.println(((int) (EduClockGame.height * 0.09765625)));
 		System.out.println("Ancho de la pantalla: "+ EduClockGame.width + " Alto de la pantalla: " + EduClockGame.height);
 	}	 	

 	public void disposse(){
		super.dispose();
		
		// Inicializamos y asignamos textura a los botones
		txrUpBtnReloj.dispose();  
		txrDownBtnReloj.dispose();
		txrBckBtnReloj.dispose(); 
		
		txrUpBtnNumeros.dispose();
		txrDownBtnNumeros.dispose();
		txrBckBtnNumeros.dispose();
		
		txrUpBtnHorera.dispose();
		txrDownBtnHorera.dispose();
		txrBckBtnHorera.dispose();
		 		
		txrUpBtnMinutera.dispose();
		txrDownBtnMinutera.dispose();
		txrBckBtnMinutera.dispose();
		
		skin.dispose();
		
		txrSonriente.dispose();
		txrTriste.dispose();
		txrFeliz.dispose();
		
		sndFeliz.dispose();
		sndTriste.dispose();
		sndPantallaSig.dispose();
		
		escenarioArmado.dispose();
		
	}
	
}
