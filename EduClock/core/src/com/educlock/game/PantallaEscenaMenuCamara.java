package com.educlock.game;

import javax.swing.JOptionPane;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.educlock.actividad.ActividadAnalogica;
import com.educlock.elemento.Jugador;
import com.educlock.elemento.Reloj;
import com.educlock.servicio.ServicioConexion;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

public class PantallaEscenaMenuCamara extends Pantalla {
	
	/* Declaramos una camara ortográfica que nos permitira ver los objetos del 
	 * mismo tamaño y así mejorar la resolución de las imagenes al redimensionar
	 * la pantalla del juego
	*/
	private OrthographicCamera cam;
	
	private Stage escenario;
	private TextButton btnSiguiente;
	private TextButton btnAtras;
	private TextButton btnSalir;
	private Skin skin;
	
	private BitmapFont bmfNombreJugador; 
	private BitmapFont bmfHoraSolicitada;
	private BitmapFont bmfPunteo;

	
	private static Texture txrFondoActividadAnalogica;
	private Texture txrSiguiente;
	private Texture txrSalir;
	
	private static Texture txrFondoReloj;
	private static Texture txrNumeroReloj;
	private static Texture txrHorera01;
	private static Texture txrHorera02;
	private static Texture txrHorera03;
	private static Texture txrHorera04;
	private static Texture txrHorera05;
	private static Texture txrHorera06;
	private static Texture txrHorera07;
	private static Texture txrHorera08;
	private static Texture txrHorera09;
	private static Texture txrHorera10;
	private static Texture txrHorera11;
	private static Texture txrHorera12;

	private static Texture txrMinutera00;
	private static Texture txrMinutera30;
	
	private static Sprite sptFondoActividadAnalogica;
	private static Sprite sptFondoReloj;
	public static Sprite sptMinutera;
	public static Sprite sptHorera;
	public static Sprite sptNumeros;
	public static Sprite sptNumeroReloj;
	
	private static String strPunteoGlobal; 
	
	// Variables para las posiciones x e y recalculadas para centrar
 	private int xWidth;
 	private int yHeight;
	 	
 	// Instancia de clases para tratamiento de datos.	
 	ActividadAnalogica actividadAnalogica = new ActividadAnalogica();
 	Reloj reloj = new Reloj();	
 	Jugador jugador = new Jugador();
 	ServicioConexion conexion = new ServicioConexion();
	 	
	 	public PantallaEscenaMenuCamara(EduClockGame game) {
	 		super(game);
	 		 		
	 		// Inicializamos el escenario 
	 		escenario = new Stage(new ExtendViewport (750, 563));
 		
	 		// Configuramos una piel o una apariencia que abarca formato de botones, fondo, tamaño, tipografía, etc.
		 	skin = new Skin(Gdx.files.internal("uiskin.json"));
		 	
	 		// Tabla para contener los botones de control
	 		Table tblControles = new Table();
	 		
	 		// Para que la tabla utilice todo el espacio del stage
	 		tblControles.setFillParent(true);
	 		
	 		Label lblNombre = new Label("Nombre", skin);
	 		Label lblPunteo = new Label("Punteo", skin);
	 		TextField txtNombre = new TextField ("", skin);
	 		
	 		escenario.addActor(tblControles);
	 		
		 	// Agregamos un boton al escenario
		 	TextButton btnAtras = new TextButton(" < ", skin);
		 	btnAtras.setPosition(745, 495);
		 	escenario.addActor(btnAtras);
		 	
		 	TextButton btnSiguiente = new TextButton(" > ", skin);
		 	btnSiguiente.setPosition(775, 495);
		 	escenario.addActor(btnSiguiente);
		 	
		 	TextButton btnSalir = new TextButton(" x ", skin);
		 	btnSalir.setPosition(805, 495);
		 	escenario.addActor(btnSalir);
	 
	 		Gdx.input.setInputProcessor(escenario);
	 			 		
	 		btnSiguiente.addListener(new InputListener(){
	 			//si hacemos clic en el actor botón, se colorea de cyan
	 			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button){
	 				System.out.println("touchDown Actor botón");
	 				return true;
	 			}
	 			
	 			public void touchUp (InputEvent event, float x, float y, int pointer, int button){
	 				System.out.println("touchUp Actor botón");
	 			}
	 		});

	 	}
	 
	 	@Override
	 	public void show () {  
	 		
	 		/*
	 		 * Inicializamos nuestras texturas, sprites y nuestras clases de funcionalidad.
	 		 */
	 		
	 		txrFondoActividadAnalogica = new Texture ("fondoActividadAnalogica/750x563/fondoActividadAnalogica.png");
	 		
	 		txrFondoReloj = new Texture ("fondoReloj/750x563/fondoReloj.png");
	 		txrNumeroReloj = new Texture ("numeroReloj/750x563/numeroReloj.png");
	 		
	 		txrMinutera00 = new Texture ("minutera/750x563/00.png");
	 		txrMinutera30 = new Texture ("minutera/750x563/30.png");
	 		
	 		txrHorera01 = new Texture ("horera/750x563/01.png");
	 		txrHorera02 = new Texture ("horera/750x563/02.png");
	 		txrHorera03 = new Texture ("horera/750x563/03.png");
	 		txrHorera04 = new Texture ("horera/750x563/04.png");
	 		txrHorera05 = new Texture ("horera/750x563/05.png");
	 		txrHorera06 = new Texture ("horera/750x563/06.png");
	 		txrHorera07 = new Texture ("horera/750x563/07.png");
	 		txrHorera08 = new Texture ("horera/750x563/08.png");
	 		txrHorera09 = new Texture ("horera/750x563/09.png");
	 		txrHorera10 = new Texture ("horera/750x563/10.png");
	 		txrHorera11 = new Texture ("horera/750x563/11.png");
	 		txrHorera12 = new Texture ("horera/750x563/12.png");
	 		
	 		txrFondoActividadAnalogica = new Texture("fondoActividadAnalogica/750x563/fondoActividadAnalogica.png");
	 
	 		sptFondoActividadAnalogica = new Sprite (txrFondoActividadAnalogica);
	 		sptFondoReloj = new Sprite (txrFondoReloj);
	 		sptNumeroReloj = new Sprite (txrNumeroReloj);
	 		sptMinutera = new Sprite (txrMinutera00);
	 		sptHorera = new Sprite (txrHorera12);
	 			
	 		bmfHoraSolicitada = new BitmapFont(); //creamos una fuente
	 		bmfPunteo = new BitmapFont();
	 		bmfNombreJugador = new BitmapFont(); 
	 		
	 		actividadAnalogica.inicializarActividadAnalogica();
	 		actividadAnalogica.emitirSolicitudSiguiente();	
	 
	 		/* A la camara ortográfica le pasaremos el alto y el ancho de la
	 		 *  pantalla
	 		 */
	 		int width = Gdx.graphics.getWidth();
	 		int height = Gdx.graphics.getHeight();
	 		
	 		cam = new OrthographicCamera(width, height);
	 	}
	 
	 	@Override
	 	public void render(float delta) {
	 		renderizarActvidadAnalogica(); 		
	 		escenario.draw();
	 		escenario.act();
	 	
	 	}
	 	
	 	public void renderizarActvidadAnalogica(){
	 		
	 		// borra la pantalla del color indicado en rgb entre 0-1 flotantes
	 		Gdx.gl.glClearColor(1, 0.8f, 0.2f, 1); 
	 		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	 		
	 		//Le pasamos la matriz de la camara al sprite batch
	 		game.batch.setProjectionMatrix(cam.combined);
	 		
	 		game.batch.begin();		// usamos el mismo sprite batch de EduClockGame	
	 		
	 		procesarEntradaTeclado();
	 		
	 		sptFondoActividadAnalogica.draw(game.batch);
	 		dibujarReloj();
	 		
	 		bmfNombreJugador.setColor(0, 0, 0, 1);
	 		bmfNombreJugador.draw(game.batch, "Panfilo Billy Filomeno Estrada", 450, 655); 
	 		
	 		bmfHoraSolicitada.setColor(0, 0, 0, 1);
	 		bmfHoraSolicitada.draw(game.batch, actividadAnalogica.arregloListaHorarioSolicitado.get(actividadAnalogica.arregloListaHorarioSolicitado.size()-1), 1000, 140);
	 		
	 		strPunteoGlobal = Integer.toString(actividadAnalogica.punteoActividad);
	 		bmfPunteo.setColor(0, 0, 0, 1);
	 		bmfPunteo.draw(game.batch, strPunteoGlobal, 870, 655); 
	 		
	 		game.batch.end();	
	 	}
	 	
	 	
	 	/* re dimensionar el tamaño de la pantalla, para que automaticamente la
	 	 * camara también cambien su tamaño. 
	 	 */
	 	
	 	@Override
	 	public void resize(int width, int height) {
	 		
	 		/* setToOrtho sirve para cambiar el tamaño de la cámara
	 		 * si el parámetro yDown es falso los ejes de cordenadas se
	 		 * inicia abajo a la izquierda de la pantalla
	 		 */
	 		cam.setToOrtho(false, width, height);
	 		
	 		xWidth = (( width - 750)/ 2);
	 		yHeight = (( height - 563)/ 2);
	 		sptFondoActividadAnalogica.setPosition(xWidth, yHeight);
	 		sptFondoReloj.setPosition(xWidth, yHeight);
	 		sptNumeroReloj.setPosition(xWidth, yHeight);
	 		sptMinutera.setPosition(xWidth, yHeight);
	 		sptHorera.setPosition(xWidth, yHeight);
	 		escenario.getViewport().update(width, height);
	 
	 		System.out.print("xWidth: "+ xWidth + "yHeight: " + yHeight);
	 	}
	 	
	private void procesarEntradaTeclado(){
	 		boolean arriba = Gdx.input.isKeyJustPressed(Input.Keys.UP);
	 		boolean abajo = Gdx.input.isKeyJustPressed(Input.Keys.DOWN);
	 		boolean derecha = Gdx.input.isKeyJustPressed(Input.Keys.RIGHT);
	 		boolean izquierda = Gdx.input.isKeyJustPressed(Input.Keys.LEFT);
	 		boolean enter = Gdx.input.isKeyJustPressed(Input.Keys.ENTER);
	 		int indiceArregloListaHoraPrimero = 0;
	 		int indiceArregloListaMinutoPrimero = 0;
	 			 		
	 		if (arriba){
	 			indiceArregloListaMinutoPrimero = actividadAnalogica.aumentarMinutera();
	 				
	 				switch (indiceArregloListaMinutoPrimero){
	 				case 0:
	 					sptMinutera.setTexture(txrMinutera00);
	 					break;
	 				case 1: 
	 					sptMinutera.setTexture(txrMinutera30);
	 					break;
	 				}
	 			
	 			System.out.println("indiceArregloListaMinutoPrimero: " + indiceArregloListaMinutoPrimero);		
	 			
	 		}else if (abajo){
	 			indiceArregloListaMinutoPrimero = actividadAnalogica.disminuirMinutera();
	 			
	 			switch (indiceArregloListaMinutoPrimero){
	 			case 0:
	 				sptMinutera.setTexture(txrMinutera00);
	 				break;
	 			case 1: 
	 				sptMinutera.setTexture(txrMinutera30);
	 				break;
	 			}
	 		
	 			System.out.println("indiceArregloListaMinutoPrimero: " + indiceArregloListaMinutoPrimero);		
	 			
	 		}else if (derecha){
	 			indiceArregloListaHoraPrimero = actividadAnalogica.aumentarHorera();
	 			switch (indiceArregloListaHoraPrimero) {
	 				case 0: 
	 					sptHorera.setTexture(txrHorera01);
	 					sptHorera.draw(game.batch);
	 					break;
	 				case 1: 
	 					sptHorera.setTexture(txrHorera02);
	 					sptHorera.draw(game.batch);
	 					break;
	 				case 2: 
	 					sptHorera.setTexture(txrHorera03);
	 					sptHorera.draw(game.batch);
	 					break;
	 				case 3: 
	 					sptHorera.setTexture(txrHorera04);
	 					sptHorera.draw(game.batch);
	 					break;
	 				case 4: 
	 					sptHorera.setTexture(txrHorera05);
	 					sptHorera.draw(game.batch);
	 					break;
	 				case 5: 
	 					sptHorera.setTexture(txrHorera06);
	 					sptHorera.draw(game.batch);
	 					break;
	 				case 6: 
	 					sptHorera.setTexture(txrHorera07);
	 					sptHorera.draw(game.batch);
	 					break;
	 				case 7: 
	 					sptHorera.setTexture(txrHorera08);
	 					sptHorera.draw(game.batch);
	 					break;
	 				case 8: 
	 					sptHorera.setTexture(txrHorera09);
	 					sptHorera.draw(game.batch);
	 					break;
	 				case 9: 
	 					sptHorera.setTexture(txrHorera10);
	 					sptHorera.draw(game.batch);
	 					break;
	 				case 10: 
	 					sptHorera.setTexture(txrHorera11);
	 					sptHorera.draw(game.batch);
	 					break;
	 				case 11: 
	 					sptHorera.setTexture(txrHorera12);
	 					sptHorera.draw(game.batch);
	 					break;
	 			}
	 			System.out.println("indiceArregloListaHoraPrimero: " + indiceArregloListaHoraPrimero);		
	 			
	 		}else if (izquierda){
	 			indiceArregloListaHoraPrimero = actividadAnalogica.disminuirHorera();
	 			switch (indiceArregloListaHoraPrimero) {
	 				case 0: 
	 					sptHorera.setTexture(txrHorera01);
	 					sptHorera.draw(game.batch);
	 					break;
	 				case 1: 
	 					sptHorera.setTexture(txrHorera02);
	 					sptHorera.draw(game.batch);
	 					break;
	 				case 2: 
	 					sptHorera.setTexture(txrHorera03);
	 					sptHorera.draw(game.batch);
	 					break;
	 				case 3: 
	 					sptHorera.setTexture(txrHorera04);
	 					sptHorera.draw(game.batch);
	 					break;
	 				case 4: 
	 					sptHorera.setTexture(txrHorera05);
	 					sptHorera.draw(game.batch);
	 					break;
	 				case 5: 
	 					sptHorera.setTexture(txrHorera06);
	 					sptHorera.draw(game.batch);
	 					break;
	 				case 6: 
	 					sptHorera.setTexture(txrHorera07);
	 					sptHorera.draw(game.batch);
	 					break;
	 				case 7: 
	 					sptHorera.setTexture(txrHorera08);
	 					sptHorera.draw(game.batch);
	 					break;
	 				case 8: 
	 					sptHorera.setTexture(txrHorera09);
	 					sptHorera.draw(game.batch);
	 					break;
	 				case 9: 
	 					sptHorera.setTexture(txrHorera10);
	 					sptHorera.draw(game.batch);
	 					break;
	 				case 10: 
	 					sptHorera.setTexture(txrHorera11);
	 					sptHorera.draw(game.batch);
	 					break;
	 				case 11: 
	 					sptHorera.setTexture(txrHorera12);
	 					sptHorera.draw(game.batch);
	 					break;
	 			}
	 			System.out.println("indiceArregloListaHoraPrimero: " + indiceArregloListaHoraPrimero);	
	 		}else if (enter){
	 			
	 			if (actividadAnalogica.contadorActividad <  6){
	 				actividadAnalogica.responderItemActividad();
	 				actividadAnalogica.emitirSolicitudSiguiente();
	 				
	 				//PRUEBA REPORTE MIGRAR A BOTON
	 				 imprimir_reporte();
	 				
	 				
	 			}else {
	 				JOptionPane.showMessageDialog(null, "Ya terminaste el nivel del juego");
	 				
	 				//PRUEBA REPORTE MIGRAR A BOTON
	 				// imprimir_reporte();

	 			}
	 			
	 		}
	 	}
	
	//Método para imprimir el reporte
	
	 @SuppressWarnings("deprecation")
	void imprimir_reporte() {

	        try {
	            JasperReport jr = (JasperReport) JRLoader.loadObject(PantallaEscenaMenuCamara.class.getResource("/com.educlock.reportes/reporte.jasper"));
	            JasperPrint jp = JasperFillManager.fillReport(jr, null, conexion.conectar());
	            JasperViewer jv = new JasperViewer(jp);
	            jv.show();
	        } catch (Exception e) {
	            System.out.print(e.getMessage());
	        }
	    }
	
	 	
	public void disposse(){
		super.dispose();

		txrFondoActividadAnalogica.dispose();	
		txrFondoReloj.dispose();
		txrNumeroReloj.dispose();
		txrHorera01.dispose();
		txrHorera02.dispose(); 
		txrHorera03.dispose();
		txrHorera04.dispose(); 
		txrHorera05.dispose(); 
		txrHorera06.dispose(); 
		txrHorera07.dispose(); 
		txrHorera08.dispose(); 
		txrHorera09.dispose(); 
		txrHorera10.dispose(); 
		txrHorera11.dispose(); 
		txrHorera12.dispose(); 
		txrMinutera00.dispose();
		txrMinutera30.dispose(); 
		txrSiguiente.dispose();
		txrSalir.dispose();
		escenario.dispose();
	}
	
	//dibujar el reloj en pantalla
	public void dibujarReloj(){
		sptFondoReloj.draw(game.batch);
		sptNumeroReloj.draw(game.batch);
		sptMinutera.draw(game.batch);
		sptHorera.draw(game.batch);
	}
	
}
