package com.educlock.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

public class PantallaEscenaDatosJugadorCamara extends Pantalla {
	
	/* Declaramos una camara ortográfica que nos permitira ver los objetos del 
	 * mismo tamaño y así mejorar la resolución de las imagenes al redimensionar
	 * la pantalla del juego
	*/
	
	private OrthographicCamera cam;
	private Stage escenario;
	private Skin skin;
	
	// Variables para las posiciones x e y recalculadas para centrar
 	private int xWidth;
 	private int yHeight;
 	 	
	 	public PantallaEscenaDatosJugadorCamara(EduClockGame game) {
	 		super(game);
	 		 		
	 		// Inicializamos el escenario 
	 		escenario = new Stage(new ExtendViewport (750, 563));
 		
	 		// Configuramos una piel o una apariencia que abarca formato de botones, fondo, tamaño, tipografía, etc.
		 	skin = new Skin(Gdx.files.internal("uiskin.json"));
		 	
	 		// Tabla para contener los botones de control
	 		Table tblDatosJugador = new Table();
	 		
	 		// Para que la tabla utilice todo el espacio del stage
	 		tblDatosJugador.setFillParent(true);
	 		
		 	// Inicializamos y asignamos el skin 
	 		Label lblNombre = new Label("Nombre:", skin);
	 		Label lblGrado = new Label("Grado:", skin);
	 		final TextField txfNombre = new TextField ("", skin);
	 		txfNombre.setName("txfNombre");
	 		final SelectBox<String>  slbGrado = new SelectBox<String>(skin);
	 		slbGrado.setItems("", "Primero", "Segundo", "Tercero", "Cuarto", "Quinto", "Sexto");
	 		
		 	TextButton btnAceptar = new TextButton("Aceptar", skin);
		 	TextButton btnCancelar = new TextButton("Cancelar", skin);
		 	
	 		tblDatosJugador.add(lblNombre).align(Align.right);
	 		tblDatosJugador.add(txfNombre).width(250).padLeft(5).padRight(5).padTop(5);
	 		tblDatosJugador.row();
	 		tblDatosJugador.add(lblGrado).align(Align.right);	 		
	 		tblDatosJugador.add(slbGrado).width(250).padLeft(5).padRight(5).padTop(5);
	 		tblDatosJugador.row();
	 		tblDatosJugador.add().height(10);
	 		tblDatosJugador.row();
	 		tblDatosJugador.add(btnAceptar).colspan(2).width(250).align(Align.right).padTop(5).padRight(5).padLeft(5);
	 		tblDatosJugador.row();
	 		tblDatosJugador.add(btnCancelar).colspan(2).width(250).align(Align.right).padTop(5).padRight(5).padLeft(5);
	 		
	 		// Esto nos servira para detectar errores y mostrar la tabla
	// 		tblDatosJugador.debug();
	 		
	 		escenario.addActor(tblDatosJugador);
 	
	 		Gdx.input.setInputProcessor(escenario);
	 		
	 		btnAceptar.addListener(new InputListener(){
	 			//si hacemos clic en el actor botón, se colorea de cyan
	 			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button){
	 				System.out.println("touchDown botón btnAceptar");
	 				
	 				// Verificamos si los campos estan con la cadena vacia
	 				if ((txfNombre.getText().equals("")) || (slbGrado.getSelected().equals(""))){
	 					System.out.println("Debe escribir su nombre y seleccionar su grado.");
	 				}else{
		 				/* De lo contrario obtenemos el valor del campo nombre y del grado para 
		 				 * configurarselo a nuestro objeto jugador
		 				 */
	 					EduClockGame.jugador.setNombreJugador(txfNombre.getText());
		 			 	System.out.println(EduClockGame.jugador.getNombreJugador());	
		 			 	
		 			 	// Y asignamos el valor del grado en entero al objeto jugador
		 			 	if (slbGrado.getSelected().equals("Primero")){
		 			 		System.out.println(slbGrado.getSelected());
			 			 	EduClockGame.jugador.setGradoJugador(1);
		 			 	}
		 			 	if (slbGrado.getSelected().equals("Segundo")){
		 			 		System.out.println(slbGrado.getSelected());
			 			 	EduClockGame.jugador.setGradoJugador(2);
		 			 	}
		 			 	if (slbGrado.getSelected().equals("Tercero")){
		 			 		System.out.println(slbGrado.getSelected());
			 			 	EduClockGame.jugador.setGradoJugador(3);
		 			 	}
		 			 	if (slbGrado.getSelected().equals("Cuarto")){
		 			 		System.out.println(slbGrado.getSelected());
			 			 	EduClockGame.jugador.setGradoJugador(4);
		 			 	}
		 			 	if (slbGrado.getSelected().equals("Quinto")){
		 			 		System.out.println(slbGrado.getSelected());
			 			 	EduClockGame.jugador.setGradoJugador(5);
		 			 	}
		 			 	if (slbGrado.getSelected().equals("Sexto")){
		 			 		System.out.println(slbGrado.getSelected());
			 			 	EduClockGame.jugador.setGradoJugador(6);
		 			 	}		 			 	
	 				}
	 			 	
	 				return true;
	 			}
	 			
	 			public void touchUp (InputEvent event, float x, float y, int pointer, int button){
	 				System.out.println("touchUp botón btnAceptar");
	 			}
	 		});
	 		
	 		btnCancelar.addListener(new InputListener(){
	 			//si hacemos clic en el actor botón, se colorea de cyan
	 			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button){
	 				System.out.println("touchDown botón btnCancelar");
	 				
	 				txfNombre.setText("");
	 				slbGrado.setSelected("");
	 				
	 				return true;
	 			}
	 			
	 			public void touchUp (InputEvent event, float x, float y, int pointer, int button){
	 				System.out.println("touchUp botón btnCancelar");
	 			}
	 		});

	 	}
	 
	 	@Override
	 	public void show () {  
	 		
	 		/* A la camara ortográfica le pasaremos el alto y el ancho de la
	 		 *  pantalla
	 		 */
	 		int width = Gdx.graphics.getWidth();
	 		int height = Gdx.graphics.getHeight();
	 		
	 		cam = new OrthographicCamera(width, height);
	 	}
	 
	 	@Override
	 	public void render(float delta) {
	 		renderizarPantallaDatosJugador(); 		
	 		escenario.draw();
	 		escenario.act();
	 	
	 	}
	 	
	 	public void renderizarPantallaDatosJugador(){
	 		
	 		// borra la pantalla del color indicado en rgb entre 0-1 flotantes
	 		Gdx.gl.glClearColor(1, 0.8f, 0.2f, 1); 
	 		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	 		
	 		//Le pasamos la matriz de la camara al sprite batch
	 		game.batch.setProjectionMatrix(cam.combined);
	 		
	 		game.batch.begin();		// usamos el mismo sprite batch de EduClockGame	
	 		
	 		procesarEntrada();	 	
	 		
	 		game.batch.end();	
	 	}
	 	
	 	
	 	/* re dimensionar el tamaño de la pantalla, para que automaticamente la
	 	 * camara también cambien su tamaño. 
	 	 */
	 	
	 	@Override
	 	public void resize(int width, int height) {
	 		
	 		/* setToOrtho sirve para cambiar el tamaño de la cámara
	 		 * si el parámetro yDown es falso los ejes de cordenadas se
	 		 * inicia abajo a la izquierda de la pantalla
	 		 */
	 		cam.setToOrtho(false, width, height);
	 		
	 		xWidth = (int) (( width - 750)/ 2);
	 		yHeight =  (int)(( height - 563)/ 2);
	 	
	 		escenario.getViewport().update(width, height);
	 
	 		System.out.print("xWidth: "+ xWidth + "yHeight: " + yHeight);
	 	}
	 	
	 	
	private void procesarEntrada(){
	 		
	 	}


	public void disposse(){
		super.dispose();

		escenario.dispose();
	}
	
}
