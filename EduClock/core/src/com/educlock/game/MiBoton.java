/**
 * 
 */
package com.educlock.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;

/**
 * @author luis
 *
 */
public class MiBoton extends ImageButton {
    
	public MiBoton(Texture texture_up, Texture texture_down, Texture background)
    {
        super(new SpriteDrawable(new Sprite(texture_up)),
              new SpriteDrawable(new Sprite(texture_down)));

        this.setBackground(new SpriteDrawable(new Sprite(background)));
    }
	
	/**
	 * @param skin
	 */
	public MiBoton(Skin skin) {
		super(skin);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param style
	 */
	public MiBoton(ImageButtonStyle style) {
		super(style);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param imageUp
	 */
	public MiBoton(Drawable imageUp) {
		super(imageUp);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param skin
	 * @param styleName
	 */
	public MiBoton(Skin skin, String styleName) {
		super(skin, styleName);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param imageUp
	 * @param imageDown
	 */
	public MiBoton(Drawable imageUp, Drawable imageDown) {
		super(imageUp, imageDown);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param imageUp
	 * @param imageDown
	 * @param imageChecked
	 */
	public MiBoton(Drawable imageUp, Drawable imageDown, Drawable imageChecked) {
		super(imageUp, imageDown, imageChecked);
		// TODO Auto-generated constructor stub
	}

}
