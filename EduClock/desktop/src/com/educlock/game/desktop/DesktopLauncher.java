package com.educlock.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.educlock.game.EduClockGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		
		config.width = 1024;
		config.height = 768;		
		config.fullscreen = false;
		
		new LwjglApplication(new EduClockGame(), config);
	}
}
